﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KutuphaneVeritabani1.Models
{
    public class RolKullaniciEkle
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}