﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Multi {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Multi() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("KutuphaneVeritabani1.Resources.Multi", typeof(Multi).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkında.
        /// </summary>
        public static string About {
            get {
                return ResourceManager.GetString("About", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string Contact {
            get {
                return ResourceManager.GetString("Contact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ekle.
        /// </summary>
        public static string Ekle {
            get {
                return ResourceManager.GetString("Ekle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to HABERLER.
        /// </summary>
        public static string haberler {
            get {
                return ResourceManager.GetString("haberler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anasayfa.
        /// </summary>
        public static string Home {
            get {
                return ResourceManager.GetString("Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İngilizce.
        /// </summary>
        public static string Ingilizce {
            get {
                return ResourceManager.GetString("Ingilizce", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kitap Ekle.
        /// </summary>
        public static string KitapEkle {
            get {
                return ResourceManager.GetString("KitapEkle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kitap Sil.
        /// </summary>
        public static string KitapSil {
            get {
                return ResourceManager.GetString("KitapSil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol Ata.
        /// </summary>
        public static string RolAta {
            get {
                return ResourceManager.GetString("RolAta", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol Ekle.
        /// </summary>
        public static string RolEkle {
            get {
                return ResourceManager.GetString("RolEkle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Roller.
        /// </summary>
        public static string Roller {
            get {
                return ResourceManager.GetString("Roller", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Türkçe.
        /// </summary>
        public static string Turkce {
            get {
                return ResourceManager.GetString("Turkce", resourceCulture);
            }
        }
    }
}
