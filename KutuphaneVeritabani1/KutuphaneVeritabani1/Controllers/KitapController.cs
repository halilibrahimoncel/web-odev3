﻿using KutuphaneVeritabani1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneVeritabani1.Controllers
{
    [Authorize]
    public class KitapController : Controller
    {
        // GET: Kitap
        KutuphaneDBEntities db = new KutuphaneDBEntities();
        public ActionResult Index()
        {

            var data = db.Kitap.ToList();
            return View(data);
        }
        public ActionResult KitapEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KitapEkle(Kitap kitap)
        {
            kitap.Tarih = DateTime.Now;
            db.Kitap.Add(kitap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult KitapSil(int id)
        {
            db.Kitap.Remove(db.Kitap.FirstOrDefault(x => x.KitapID == id));
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //public ActionResult KitapDuzenle(int id)
        //{
        //    return View(db.Kitap.FirstOrDefault(x => x.KitapID == id));
        //}
        //[HttpPost]
        //public ActionResult KitapDuzenle(Kitap kitap)
        //{
        //    db.Entry(kitap).State = System.Data.Entity.EntityState.Modified;
        //    kitap.Tarih = DateTime.Now;
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}