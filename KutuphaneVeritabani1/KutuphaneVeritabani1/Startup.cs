﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(KutuphaneVeritabani1.Startup))]
namespace KutuphaneVeritabani1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
