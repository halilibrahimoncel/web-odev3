﻿using KutuphaneVeritabani1.Areas.Admin.Models;
using KutuphaneVeritabani1.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KutuphaneVeritabani1.Areas.Admin.Controllers
{
    public class RolYonetimController : Controller
    {
        ApplicationDbContext context = new ApplicationDbContext();
        // GET: Admin/RolYonetim
        public ActionResult Index()
        {
            var RoleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(RoleStore);
          var model=  roleManager.Roles.ToList();
            return View(model);
        }
        public ActionResult RolEkle()
        {
         
            return View();
        }
        [HttpPost]
        public ActionResult RolEkle(RolEkleModel rol)
        {
            var RoleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(RoleStore);
           if( roleManager.RoleExists(rol.RolAd)==false)
            {
                roleManager.Create(new IdentityRole(rol.RolAd));
            }
            return RedirectToAction("Index");
        }
        public ActionResult RolKullaniciEkle()
        {
            return View();
        }
        [HttpPost]
        public ActionResult RolEkle(RolKullaniciEkleModel model)
        {
            var RoleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(RoleStore);
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);


          var kullanici= userManager.FindByName(model.KullaniciAdi);
            userManager.AddToRole(kullanici.Id,model.RolAdi);
            if (!userManager.IsInRole(kullanici.Id,model.RolAdi))
            {
                userManager.AddToRoles(kullanici.Id, model.RolAdi);
            }
            return RedirectToAction("Index");
        }
        
    }
}