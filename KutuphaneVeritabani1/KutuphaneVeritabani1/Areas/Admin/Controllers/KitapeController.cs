﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KutuphaneVeritabani1.Areas.Admin.Models;

namespace KutuphaneVeritabani1.Areas.Admin.Controllers
{
    public class KitapeController : Controller
    {
        private KutuphaneVeritabaniEntities db = new KutuphaneVeritabaniEntities();

        // GET: Admin/Kitape
        public ActionResult Index()
        {
            return View(db.Kitapes.ToList());
        }

        // GET: Admin/Kitape/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kitape kitape = db.Kitapes.Find(id);
            if (kitape == null)
            {
                return HttpNotFound();
            }
            return View(kitape);
        }

        // GET: Admin/Kitape/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Kitape/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,KitapAd,Basimyili,Yazar,RafSiraNo,Binakat")] Kitape kitape)
        {
            if (ModelState.IsValid)
            {
                db.Kitapes.Add(kitape);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kitape);
        }

        // GET: Admin/Kitape/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kitape kitape = db.Kitapes.Find(id);
            if (kitape == null)
            {
                return HttpNotFound();
            }
            return View(kitape);
        }

        // POST: Admin/Kitape/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,KitapAd,Basimyili,Yazar,RafSiraNo,Binakat")] Kitape kitape)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kitape).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kitape);
        }

        // GET: Admin/Kitape/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kitape kitape = db.Kitapes.Find(id);
            if (kitape == null)
            {
                return HttpNotFound();
            }
            return View(kitape);
        }

        // POST: Admin/Kitape/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kitape kitape = db.Kitapes.Find(id);
            db.Kitapes.Remove(kitape);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
